#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 11:44:25 2020

@author: mevius
"""


import numpy as np
from scipy.optimize import least_squares as lstsq


def fit_mvdr(beam, sat_dir, rfi_dir, rfi_power,freq, 
             receiver_pol = 'ele', transmit_pol = 'rcp', tpol_rfi = None):
    '''
    Maximize S/N ration of Satelite signal, given rfi_dir/rfi_power.
    Based on code Bas vd Tol 

    Parameters
    ----------
    beam : pabeam.BeamFormer
        Beam object
    sat_dir : array (2)
        direction of interest (theta,phi in degrees)
    rfi_dir : array of float (2,N)
        direction of RFI sources to be suppressed (theta,phi in degrees)
    rfi_power : array of float (N)
        power of the RFI sources to be suppressed
    freq : float
            Frequency (Hz)

    Returns
    -------
    array of complex
    Set of weights for the beam

    '''
    if tpol_rfi is None:
        tpol_rfi = transmit_pol
    a = beam.getH(sat_dir[0], sat_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    g = beam.getG(sat_dir[0], sat_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    A = beam.getH(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = tpol_rfi)
    G = beam.getG(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = tpol_rfi)
    W = G*A  #Nant x N_rfi
    if len(rfi_power.shape)<2:
        rfi_power = np.diag(rfi_power)
    R = np.dot(np.dot(W, rfi_power), W.T.conj()) #+ np.eye(beam.Nant)
    Rinv = np.linalg.inv(R)
    w = np.dot(Rinv, a*g)  
    w /= np.dot(np.conj(w.T), a*g)
    return w





def fit_mvdr_multisat(beam, sat_dir, sat_power, rfi_dir, rfi_power,freq,
                      receiver_pol = 'ele', transmit_pol = 'rcp', 
                      tpol_rfi = None):
    '''
    Maximize S/N ration of Satelite signal, given rfi_dir/rfi_power.
    Based on code Bas vd Tol 

    Parameters
    ----------
    beam : pabeam.BeamFormer
        Beam object
    sat_dir : array (2,M)
        direction of interest (theta,phi in degrees)
    sat_power : array of float (M)
        constraint for the individual satelites
    rfi_dir : array of float (2,N)
        direction of RFI sources to be suppressed (theta,phi in degrees)
    rfi_power : array of float (N)
        power of the RFI sources to be suppressed
    freq : float
            Frequency (Hz)

    Returns
    -------
    array of complex
    Set of weights for the beam

    '''
    if tpol_rfi is None:
        tpol_rfi = transmit_pol
    a = beam.getH(sat_dir[0], sat_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    g = beam.getG(sat_dir[0], sat_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    A = beam.getH(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = tpol_rfi)
    G = beam.getG(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = tpol_rfi)
    W = G*A  #Nant x N_rfi (x2)
    Ws = g*a  #Nant x N_sat
    if len(rfi_power.shape)<2:
        rfi_power = np.diag(rfi_power)
    R = np.dot(np.dot(W, rfi_power), W.T.conj()) #+ np.eye(beam.Nant)
    Rinv = np.linalg.inv(R)
    w = np.dot(Rinv, Ws)
    #w = np.dot(w, np.linalg.inv( np.dot (Ws.T.conj(), w)))
 
    w = np.dot(w,np.linalg.pinv(np.dot(np.dot(Ws.conj().T,Rinv),Ws)))
    w = np.dot(w, sat_power)
    
    return w



def projection(beam, rfi_dir, freq, 
               receiver_pol = 'ele', transmit_pol = 'rcp'):
    A = beam.getH(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    G = beam.getG(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    W = G*A  #Nant x N_rfi
    Nant = W.shape[0]
    Nrfi = W.shape[1]
    P = np.eye(Nant)
    R = np.dot(W,W.conj().T)
    l,V = np.linalg.eig(R)
    V_sel = V[:,np.abs(l)>1e-7]
    #print(V,V_sel.shape,V_sel,l)
    P = P - np.dot(V_sel,V_sel.conj().T)
    # for i in range(Nrfi):
    #     #wtemp = np.dot(W[:,i],P)
    #     #p = np.eye(beam.Nant) - \
    #     #    np.outer(wtemp,wtemp.conj())/np.dot(wtemp,wtemp.conj())
    #     #P = p
    #     p = np.eye(beam.Nant) - \
    #         np.outer(W[:,i],W[:,i].conj())/np.dot(W[:,i],W[:,i].conj())
    #     P = np.dot(p,P)

    return P

def Projfit(beam,sat_dir, rfi_dir,freq,
            receiver_pol = 'ele', transmit_pol = 'rcp', tpol_rfi = None):
    if tpol_rfi is None:
        tpol_rfi = transmit_pol
    a = beam.getH(sat_dir[0], sat_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    g = beam.getG(sat_dir[0], sat_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    P = projection(beam, rfi_dir,freq,receiver_pol = receiver_pol, 
                   transmit_pol = tpol_rfi )
    Ws = g*a #Nant x 1
    return np.dot(Ws,P)
    
        

def NLLfit(beam,source_dir,constraints,freq, GF = True, AF = True ,
           receiver_pol = 'ele', transmit_pol = 'rcp', tpol_rfi = None):
    '''
        Non linear Least square fit of weights optimizing beam power 
        given a set of contraints on theta, phi grid
        TODO: Fit optimal beam for all frequencies?
        Parameters
        ----------
        source_dir : array of float  (2,N)
            angles theta,phi (degrees) of N sources
        constraints : array of float (N)
            constraint values on theta/phi grid.
        freq : float
            Frequency (Hz)

        Returns
        -------
        array of complex:
            Complex weights

    '''
    if tpol_rfi is None:
        tpol_rfi = transmit_pol
    a = beam.getH(source_dir[0], source_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    g = beam.getG(source_dir[0], source_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    A_norm = beam.getH(0,0, freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    G_norm = beam.getG(0,0, freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
    if GF and AF:
        G_norm = np.dot(A_norm,G_norm)
        A = (a * g)  # Nant,Nsrc
    elif GF:
        A = g
    else:
        A = a
        G_norm = A_norm
    #A = A/(np.sum(np.abs(G_norm)))
    Aht  = np.conj(A.T)
    def F(x):
        w = x.reshape((2,-1))[0]+1.j*x.reshape((2,-1))[1] #create complex number
        #w /= np.sum(np.abs(w))
        G = np.abs(np.dot(Aht,w))
        #normalize weights?
#        return np.abs(G)**2 - constraints 
        return np.abs(G - np.abs(constraints))**2 

    w = np.ones(A.shape[0]*2)  #lsttsq only accepts real numbers
    w[w.shape[0]//2:]=0
    w =  lstsq(F,w)['x']
    w = w.reshape((2,-1))[0]+1.j*w.reshape((2,-1))[1]
    return w
        
def FitBartlett(beam, source_dir, freq,
               receiver_pol = 'ele', transmit_pol = 'rcp'):
         '''
        Least square fit of weights optimizing beam for directions 
        in source_dir
   
        Parameters
        ----------
        source_dir : array of float  (2,N)
            angles theta,phi (degrees) of N sources
        freq : float
            Frequency (Hz)

        Returns
        -------
        array of complex:
            Complex weights

        '''
   
         A = beam.getA(source_dir[0], source_dir[1], freq, 
                      pol = receiver_pol, transmit_pol = transmit_pol)
         w = A
         w /= np.sum(np.abs(w))
         return w.squeeze()
   
    

def FitWeightsHOnly(beam, source_dir, constraints, freq,
               receiver_pol = 'ele', transmit_pol = 'rcp'):
        '''
        Least square fit of weights given a set of contraints on theta, phi grid
        TODO: Fit optimal beam for all frequencies?
        Parameters
        ----------
        source_dir : array of float  (2,N)
            angles theta,phi (degrees) of N sources
        constraints : array of float (N)
            constraint values on theta/phi grid.
        freq : float
            Frequency (Hz)

        Returns
        -------
        array of complex:
            Complex weights

        '''
        A = beam.getH(source_dir[0], source_dir[1], freq, 
                      pol = receiver_pol, transmit_pol = transmit_pol)
        Aht  = np.conj(A.T)
        constraints = constraints.reshape(constraints.shape[0],1)
        
        if constraints.shape[0] >= A.shape[0]:
            print("overdetermined")
            return np.dot(np.linalg.pinv(Aht),constraints)  #complex weights
        else:
            print("underdetermined")
            return np.dot(np.linalg.pinv(Aht),constraints)  #complex weights

def FitWeights(beam, source_dir, constraints, freq,
               receiver_pol = 'ele', transmit_pol = 'rcp'):
        '''
        Least square fit of weights given a set of contraints on theta, phi grid
        TODO: Fit optimal beam for all frequencies?
        Parameters
        ----------
        source_dir : array of float  (2,N)
            angles theta,phi (degrees) of N sources
        constraints : array of float (N)
            constraint values on theta/phi grid.
        freq : float
            Frequency (Hz)

        Returns
        -------
        array of complex:
            Complex weights

        '''
        A = beam.getH(source_dir[0], source_dir[1], freq, 
                      pol = receiver_pol, transmit_pol = transmit_pol)
        g = beam.getG(source_dir[0], source_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol)
        A = (A*g)
        #A_norm = beam.getA(0,0, freq, 
        #                   pol = receiver_pol, transmit_pol = transmit_pol)
        #A /= np.abs(A_norm)[:,np.newaxis]
        Aht  = np.conj(A.T)
        constraints = constraints.reshape(constraints.shape[0],1)
        if constraints.shape[0] >= A.shape[0]:
            print("overdetermined")
            return np.dot(np.linalg.pinv(Aht),constraints).squeeze()  #complex weights
        else:
            print("underdetermined")
            return np.dot(np.linalg.pinv(Aht),constraints).squeeze()  #complex weights

def projectionG(beam, rfi_dir, freq, 
               receiver_pol = 'ele', transmit_pol = 'rcp'):
    W = beam.getG(rfi_dir[0], rfi_dir[1], freq, 
                  pol = receiver_pol, transmit_pol = transmit_pol) #Nant x N_rfi
    Nant = W.shape[0]
    Nrfi = W.shape[1]
    P = np.eye(Nant)
    R = np.dot(W,W.conj().T)
    l,V = np.linalg.eig(R)
    V_sel = V[:,np.abs(l)>1e-7]
    #print(V,V_sel.shape,V_sel,l)
    P = P - np.dot(V_sel,V_sel.conj().T)
    # for i in range(Nrfi):
    #     #wtemp = np.dot(W[:,i],P)
    #     #p = np.eye(beam.Nant) - \
    #     #    np.outer(wtemp,wtemp.conj())/np.dot(wtemp,wtemp.conj())
    #     #P = p
    #     p = np.eye(beam.Nant) - \
    #         np.outer(W[:,i],W[:,i].conj())/np.dot(W[:,i],W[:,i].conj())
    #     P = np.dot(p,P)

    return P

