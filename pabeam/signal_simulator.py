#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:50:58 2020

@author: mevius
"""
from pabeam import beamformer as bf
import numpy as np
from scipy.constants import speed_of_light,Boltzmann


# define ssignal vector (as seen by the receiver)
signal_dict = bf.signal_dict

#function to create random complex timeseries of lenght x
make_ts = make_time_series = lambda x:1./np.sqrt(2.)*((np.random.normal(0, 1., x) 
                                        + 1.j*np.random.normal(0, 1., x)))

def create_timeseries_per_antenna(mybeam, source_dir, freq, timeseries,
                                  pol_signal = 'rcp',pol_receiver = 'rcp',
                                  no_gain=False):
    '''
    Create signal per antenna/element (depending on choice of pol_receiver)

    Parameters
    ----------
    mybeam : pabeam.beamformer
        DESCRIPTION.
    source_dir : array of float (2)
        theta phi of the source
    freq : float
        Frequency (MHz) for which to analyse the beam
    timeseries : array of complex
        complex timeseries (filtered) of the signal
    pol_signal : string
        any of lcp,rcp,hor,vert,none
    pol_receiver : string
        any of lcp,rcp,hor,vert,circ,lin
    
    Returns
    -------
    array of float  (Nant x Ntimes)

    '''
    H = mybeam.getH(source_dir[0],source_dir[1], freq, pol_receiver).squeeze()
    G = mybeam.getG(source_dir[0],source_dir[1], freq, pol_receiver).squeeze()
    #G = mybeam.InterpolateGains(source_dir[0], source_dir[1], freq,
     #                              gains = mybeam.gain_dict[pol_receiver])
    #ft_time = np.fft.rfft(timeseries)
    BEP = 1./mybeam.Z0
    Aeff = BEP*120*np.pi
    abs_gain = np.sqrt(4*np.pi*Aeff*freq**2/(speed_of_light)**2)
    #print(abs_gain)
    
    pol_signal = timeseries[np.newaxis] * signal_dict[pol_signal][:,np.newaxis]
    #pos singal has shape 2 x nTimes
    #G*H has shape Nant/elem x 2
    signal = np.dot(abs_gain*(H*G), pol_signal)
    if no_gain:
        signal = np.dot(H,pol_signal)
    return signal

def create_noise_per_antenna(Nant = 7, Tsys = 550., bandwidth = 25e6, Nsam =1):
    noise_power = np.sqrt(Tsys*Boltzmann*bandwidth)  # Tsys*k_B*bandwidth
    # noise_power = np.sqrt(70.*Boltzmann*25e6)  # Tsys*k_B*bandwidth
    
    N = np.array([noise_power*make_ts(Nsam) for i in range(Nant)])
    
    return N

def create_timeseries_from_file(fname,sample_rate = 5000000, T=1):
    myf = open(fname,'rb')
    timeseries = np.fromfile(myf, dtype=np.complex64, 
                             count=int(T*sample_rate))
    return timeseries
    
def create_gaussian_timeseries(samples,sample_rate, freq,
                               sigma=1., bandwidth = 25.e6): 
   cdata = np.zeros(samples,dtype = np.complex64)
   myfilter = [int((1./sample_rate)*(freq*1e-6 - freq_bw*1e-6/2)),
               int((1./sample_rate)*(freq *1e-6+ freq_bw*1e-6/2))]
   nfreq_samples = myfilter[1]-myfilter[0]
   real = np.random.normal(nfreq_samples)*sigma
   imag = np.random.normal(nfreq_samples)*sigma
   cdata[myfilter[0]:myfitler[1]] = real+1.j*imag
   return np.fft.irfft(cdata)
   
def calculate_corr_matrix(timeseries_per_antenna, kernel=1):
    Z = timeseries_per_antenna
    C = np.dot(Z*kernel,Z.T.conj())
    return C

def generate_prn():
    nbits=1023
    code = np.zeros(nbits,dtype=int)
    
def DAM(signal, nsam):
    return signal[nsam:]*np.conj(signal[:-nsam])
    

