"""
Created on Wed Aug 26 13:00:27 2020

@author: mevius
"""

import numpy as np
from scipy.constants import speed_of_light,Boltzmann

def sphere2cart(theta,phi):
    '''
    convert spherical coordinates to cartesian unit coordinates

    Parameters
    ----------
    theta : (array of) float
        polar angle theta (w.r.t. zenith) in rad
    phi : (array of) float
        azimuth angle phi in rad

    Returns
    -------
    array of float
         cartesian direction vectors S, dim.(3,) + shape (theta)   

    '''
    
    return np.array([np.cos(phi)*np.sin(theta),
                     np.sin(phi)*np.sin(theta),
                     np.cos(theta)])
    
# define ssignal vector (as seen by the receiver)
signal_dict = {}
signal_dict['hor'] = np.array([1,0.])
signal_dict['vert'] = np.array([0,1.])
signal_dict['rcp'] = np.array([1,1.j])/np.sqrt(2.)
signal_dict['lcp'] = np.array([1,-1.j])/np.sqrt(2.)
signal_dict['none'] = np.array([1,1.])/2.


class BeamFormer:
    def __init__(self, f0 = 1.5e9):
        self.f0  = f0      #reference frequency
        self.weights = 1   #weights vector (complex) for beamfomring
        self.gain = 1     #Gain matrix (Nant x Nphi X Ntheta) (x2 for xy?)
        self.theta_gain = 90
        self.phi_gain = 0
        self.gain_errors = 1
        self._has_correlation =  False
        self._has_xy_gain = False
        self.rhcp = True
        self.Z0=1
        
    def CalculateCircGain(self,G,rhcp=True):
        if self._has_xy_gain:
            rsign =(-1)**int(rhcp)
            G = G[0] + rsign*1.j*G[1]
        return G




    def GetBeamValue(self,theta,phi,freq):
        '''
        Calculate Beam Value for a given direction and frequency given a 
        set of predefined weights

        Parameters
        ----------
        theta : float
            angle (degrees)
        phi : float
            angle (degrees)
        freq : float
            freq (Hz)

        Returns
        -------
        float
            

        '''
        
        G = self.InterpolateGains(theta,phi,freq)
        if self._has_xy_gain:
            G = self.CalculateCircGain(np.moveaxis(G,-1,0),self.rhcp)
        A = self.CalculateArrayFactors(theta,phi,freq)
        w = self.weights
        e = self.gain_errors
        if A.ndim>2:
            if not np.isscalar(w):
                w = w.reshape(w.shape + (1,)*(A.ndim-2))
            if not np.isscalar(e):
                e = e.reshape(e.shape + (1,)*(A.ndim-2))
        return np.sum(w * e * G * A,axis=0)

        
    def CalculateArrayFactors(self,theta,phi,freq):
        '''Calculate the arrayfactor for all elements
        for every direction of a predefined grid(theta,phi)
        Parameters
        ----------
        theta : float
            angle (degrees)
        phi : float
            angle (degrees)
        freq : float
            freq (Hz)
        

        Returns
        -------
        array (complex): dimsensions Nant,Ntheta,Nphi
        '''
        S = sphere2cart(np.radians(theta),np.radians(phi))
        if S.ndim > 2:
            S = np.moveaxis(S,0,-2)
        pos = self.pos
        if not np.isscalar(freq):
            freq = freq.reshape(freq.shape + (1,)*((np.dot(pos,S)).ndim))
        array_factor = np.exp(2*np.pi*1j
                              * np.dot(pos,S)
                              *freq/speed_of_light) 
        #remove minus sign, since this seems to be the order in M. Arts data
        return array_factor
    
    def setArrayConfig(self,pos):
        self.pos = pos
        self.Nant = self.pos.shape[0]

    def setWeights(self,weights):
        self.weights = weights

    def setGainErrors(self,gain_errors):
        self.gain_errors = gain_errors
        
    def setGain(self,gain,theta,phi,freq, xypol = False):
        '''Set the gains of the individual antennas
        
        Keyword arguments:
            gain -- array, dimensions Nant x 2 x Ntheta x Nphi x Nfreq
            theta -- array, dimensions Ntheta
            phi -- array, dimensions Nphi
        '''
        self.gain = gain
        self.theta_gain = theta
        self.phi_gain = phi
        self.freq_gain = freq
        self._has_xy_gain = xypol

    def setCC(self,cc, Z0 =500):
        '''Set the noise correlation matrix of the individual antennas
        
        Keyword arguments:
            cc -- array, dimensions Nant x Nant x Nfreq_gain
        '''
        self.cc = cc
        self.Z0 = Z0
        self._has_correlation =  True

    def getCC(self,freq):
        if not self._has_correlation:
            self.Z0=1
            return np.eye(self.Nant)
        else:
            ifreq = np.argmin(np.abs(self.freq_gain - freq))
            return self.cc[:,:,ifreq]
        
    def InterpolateGains(self,theta,phi,freq,
                         method = "nearest_neighbour",gains = None):
        '''
        Calculate interpolated Gain values for a (pairs of) theta, phis
        and a given frequency

        Parameters
        ----------
        theta : (array of) float
            angle (degrees) same shape as phi 
        phi : (array of) float
            angle (degrees) same shape as theta
        freq: float
            frequency (Hz)
        method : string, optional
            Interpolation method. The default is "nearest_neighbour".
        gains : array of complex or None. If not specified, self.gain is used
            last three axes should be theta x phi x freq
        Returns
        -------
        array of  complex
        Complex gain value(s)

        '''
        if gains is None:
            gains = self.gain
        if np.isscalar(gains):
            return gains
        if method == "nearest_neighbour":
            thetas = self.theta_gain
            phis = self.phi_gain
            freqs = self.freq_gain
            theta = np.array(theta)
            phi = np.array(phi)
            if theta.ndim >= 1:
                    thetas = thetas.reshape(thetas.shape + theta.ndim*(1,))
                    phis = phis.reshape(phis.shape + phi.ndim*(1,))
            idx_theta = np.argmin(np.abs(thetas - theta),axis=0)
            idx_phi = np.argmin(np.abs(phis - phi),axis=0)
            idx_freq =  np.argmin(np.abs(freqs - freq))
            return gains[...,idx_theta,idx_phi,idx_freq]
        # TO DO: implement real interpolation using e.g. 
        # LSQSphereBIVariateSpline
        else: 
            print ("Other beam interpolation not implemented yet")
            return 0

    def addPhaseErrors(self,errors):
        '''
        Add phase errors for the elements

        Parameters
        ----------
        errors : array of complex (Nant)
            amplitude and phase errors

        Returns
        -------
        None.

        '''
        self.gain_errors = errors
 
    def getAbsoluteGain(self, AF ,freq):
        '''
        Calculate absolute gain given noise correlation matrix and
        impedance Z0. Calculations based on document M.Arts

        Parameters
        ----------
        AF : array of complex
            Beam voltage pattern
        freq : float
            frequency Hz

        Returns
        -------
        abs_gain : array of float
            Absolute beam power

        '''
        cc = self.getCC(freq)
        ARG = np.abs(np.dot(np.dot(self.weights,cc),
                         self.weights.conj() )) /Boltzmann      
        BEP = np.abs(AF**2/self.Z0)/ARG
        abs_gain = 4 * np.pi *BEP*(120*np.pi)*freq**2/speed_of_light**2
        return abs_gain
    
    def phaseRotate(self,theta,phi):
        # rotate phases to central reference frame
        Bz = np.cos(theta)
        Bx = np.sin(theta)*np.cos(phi)
        By = np.sin(theta)*np.sin(phi)
        Tz = -np.sin(theta)
        Tx = np.cos(theta)*np.cos(phi)
        Ty = np.cos(theta)*np.sin(phi)
        PhX=By*Tz-Bz*Ty #Calcalute using cross products of other 2 directions
        PhY=Bz*Tx-Bx*Tz
        PhZ=Bx*Ty-By*Tx #this should be 0
        X2R=Tx+1j*PhX
        Y2R=-(Ty+1j*PhY)   
        return np.concatenate((X2R[np.newaxis],Y2R[np.newaxis]),axis=0)
    
class GriddedBeam(BeamFormer):
    def __init__(self,thetas = np.linspace(0,90,50),
                 phis = np.linspace(0,360,200),
                 f0 = 1.5e9):
        '''
        Initializes beamformer with on a fixed grid

        Returns
        -------
        None.

        '''        
        self.f0  = f0      #reference frequency
        self.weights = 1   #weights vector (complex) for beamfomring
        self.gain = 1     #Gain matrix (Nant x Nphi X Ntheta)
        self.theta_gain = 90
        self.phi_gain = 0
        self.gain_errors = 1
        self.setGrid(thetas,phis)
        self._has_correlation =  False
        self._has_xy_gain = False
        self.rhcp = True
        self.Z0=1

        
    def setWeights(self,weights):
        self.weights = weights
        self._gridded_beam_calculated = False


    def setGainErrors(self,gain_errors):
        self.gain_errors = gain_errors
        self._gridded_beam_calculated = False
        
    def setGrid(self,thetas,phis):
        '''
        Defines the standard grid to calculate the beam (for fast indexing)

        Parameters
        ----------
        thetas : array of float
            THetas of grid (degrees)
        phis : array of float
            Phis of grid (degrees)


        Returns
        -------
        None.

        '''
        self.spherical_grid = np.meshgrid(phis,thetas)[::-1]
        self.thetas = thetas
        self.phis = phis
        self.grid = sphere2cart (np.radians(self.spherical_grid[0]),
                                 np.radians(self.spherical_grid[1]))
        self._gridded_beam_calculated = False

    def getBeamValuefromGrid(self,theta,phi,freq):
        ''' Get value of beam at specific theta phi


        Parameters
        ----------
        theta : float
            angle
        phi : float
            angle
        freq: float
            frequency

        Returns
        -------
        float
            beam value

        '''
        if not self._grided_beam_calculated:
            self.CalculateBeam()
        idx_theta = np.argmin(np.abs(self.thetas - theta))
        idx_phi = np.argmin(np.abs(self.phis - phi))
        idx_f = np.argmin(np.abs(self.freqs - freq))
        return self.beam[idx_theta,idx_phi,idx_f]

    def getBeamGrid(self):
        '''Get the value of the beam at the given grid
        

        Returns
        -------
        array(float): dimensions Ntheta,Nphi,Nfreq

        '''
        if not self._gridded_beam_calculated:
            self.CalculateBeam()
        return self.beam
    
    def CalculateBeam(self):
        '''Generate the full beam, array_factor . Gains
        

        Returns
        -------
        None.

        '''
        S = self.grid
        w = self.weights
        e = self.gain_errors
        if S.ndim>1:
            if not np.isscalar(w):
                w = w.reshape(w.shape + (1,)*(S.ndim-1))
            if not np.isscalar(e):
                e = e.reshape(e.shape + (1,)*(S.ndim-1))
        self.array_factor = self.CalculateArrayFactors(
            *self.spherical_grid,self.f0)
        self.interpolated_gains = self.InterpolateGains(
            *self.spherical_grid,self.f0)
        if self._has_xy_gain and not np.isscalar(w*e):
            G = np.moveaxis(self.interpolated_gains,-1,0)
            we = np.swapaxes(w*e,1,0)
            G = self.CalculateCircGain(G*we.conj(),self.rhcp)
        else:
            G=self.interpolated_gains*w.conj()*e

        self.beam = np.sum(self.array_factor*G,axis=0) #sum over Nant
        self._gridded_beam_calculated = True
        
    def getAbsoluteGain(self, AF = None ,freq = 1e9):
        '''
        Calculate absolute gain given noise correlation matrix and
        impedance Z0. Calculations based on document M.Arts

        Parameters
        ----------
        AF : array of complex
            Beam voltage pattern
        freq : float
            frequency Hz

        Returns
        -------
        abs_gain : array of float
            Absolute beam power

        '''
        self.f0 = freq
        if AF is None:
            AF  = self.getBeamGrid()
        return BeamFormer.getAbsoluteGain(self,AF, freq)        
    
class DualDipoleBeam(GriddedBeam):
    '''A phased array beam with antennas consisting of x and y polarisations'''
    def __init__(self,thetas = np.linspace(0,90,50),
                 phis = np.linspace(0,360,200),
                 f0 = 1.5e9):
        '''
        Initializes beamformer with on a fixed grid

        Returns
        -------
        None.

        '''        
        self.f0  = f0      #reference frequency
        self.weights = 1   #weights vector (complex) for beamfomring
        self.gain = 1     #Gain matrix (Nant x Nphi X Ntheta)
        self.theta_gain = 90
        self.phi_gain = 0
        self.gain_errors = 1
        self.setGrid(thetas,phis)
        self._has_correlation =  False
        self.gain_dict = {}
        self.select_pol = {}
        self.Z0=1

        
        
        
    def setGainAll(self,gain_circ, gain_lin,thetas,phis,freq):
        '''Set the gains of the individual antennas
        
        Keyword arguments:
            gain_circ -- array, dimensions Nelem x 2 x Ntheta x Nphi x Nfreq
                expected order is lcp then rcp
            gain_lin -- array, dimensions Nelem x 2 x Ntheta x Nphi x Nfreq
                expected order is hor,vert interlaced
            theta -- array, dimensions Ntheta
            phi -- array, dimensions Nphi
        '''
        self.gain_circ = self.gain_dict['circ'] = gain_circ
        self.select_pol['lcp'] = np.arange(gain_circ.shape[0]//2)
        self.select_pol['rcp'] = np.arange(gain_circ.shape[0]//2,gain_circ.shape[0])
        self.select_pol['vert'] = np.arange(1,gain_circ.shape[0],2)
        self.select_pol['hor'] = np.arange(0,gain_circ.shape[0],2)
        self.select_pol['circ'] = np.arange(gain_circ.shape[0])
        self.select_pol['ele'] = np.arange(gain_circ.shape[0])
        
        self.gain_dict['lcp'] = gain_circ[self.select_pol['lcp']]
        self.gain_dict['rcp'] = gain_circ[self.select_pol['rcp']]
        self.gain_lin =  self.gain_dict['ele'] = gain_lin
        self.gain_dict['vert'] = gain_lin[1::2]
        self.gain_dict['hor'] = gain_lin[::2]
        self.theta_gain = thetas
        self.phi_gain = phis
        self.freq_gain = freq
        self._has_xy_gain = False
        self.gain = self.gain_circ[self.Nant:,0]/np.sqrt(2.) + \
                    1.j*self.gain_circ[self.Nant:,1]/np.sqrt(2.)
                    # for backward compatibility store rhcp gains
        self.rhcp = True
                

    def  setCCAll(self,cc,cc_lin,Z0):
        self.setCC(cc,Z0)
        self.cc_lin = cc_lin
        
        
    def getH(self,thetas,phis,freq, pol = 'ele', transmit_pol = 'ele'):
        '''Returns matrix H as defined in "GNSS anti jammin and spoofing"
        
        ----------
        Keyword arguments:
        theta : array of float
            angle (degrees)
        phi : array of float
            angle (degrees)
        freq : float
            freq (Hz)
         pol: string
            'ele','rcp','lcp','hor','vert'
       

        Returns
        -------
        array (complex): dimsensions Nelem,2,Ntheta,Nphi

        
        '''
        AF = self.CalculateArrayFactors(thetas, phis, freq)
        if self.gain_dict.keys() and (not pol.lower() in self.gain_dict.keys()):
            raise Exception('polarization keyword not recognized',pol)
            
        if pol.lower() == 'ele':
            # kron product creating matrix with shape Nelem x 2 x ntheta,nphi
            H = np.kron(AF.T[...,np.newaxis,:], np.ones((2,2))).T
        else:
            if pol.lower() == 'circ':
                #first lcp then rcp
                AF = np.concatenate((AF,AF),axis=0)
            H = np.concatenate((AF[:,np.newaxis],AF[:,np.newaxis]),axis=1)
        if transmit_pol.lower() in ['ele','circ']:
            return H
        else:
#            return np.dot(H.T.swapaxes(-1,-2), signal_dict[transmit_pol]).T
            return H[:,0]
    
    def getG(self,thetas,phis,freq, pol = 'ele', transmit_pol = 'ele'):
        '''Returns gain matrix G. default is 'ele' as defined in 
        "GNSS anti jammin and spoofing"
        
        ----------
        Keyword arguments:
        theta : array of float
            angle (degrees)
        phi : array of float
            angle (degrees)
        freq : float
            freq (Hz)
        pol: string
            'ele','rcp','lcp','hor','vert'

        Returns
        -------
        array (complex): dimsensions Nelem,2,Ntheta,Nphi
        
        '''
        if np.isscalar(self.gain):
            return np.array((self.gain))
        if not pol.lower() in self.gain_dict.keys():
            raise Exception('polarization keyword not recognized',pol)
            
        gains = self.gain_dict[pol]
        
        G = self.InterpolateGains(thetas,phis,freq, gains = gains)
        if transmit_pol.lower() in ['ele','circ']:
            return G
        else:
            polrot =  np.dot(self.phaseRotate(np.radians(thetas),
                                              np.radians(phis)).swapaxes(-1,0), 
                             signal_dict[transmit_pol])
            return (np.exp(-1.j*np.angle(polrot)).T)[np.newaxis]*\
                np.dot(G.T.swapaxes(-1,-2), signal_dict[transmit_pol]).T
            #return (np.dot(G.T.swapaxes(-1,-2), signal_dict[transmit_pol])).T

    
    def getA(self,thetas,phis,freq,pol = 'ele', transmit_pol = 'ele'):
        '''Returns matrix A as defined in "GNSS anti jammin and spoofing"
        
        ----------
        Keyword arguments:
        theta : array of float
            angle (degrees)
        phi : array of float
            angle (degrees)
        freq : float
            freq (Hz)
        

        Returns
        -------
        array (complex): dimsensions Nelem,2,Ntheta,Nphi
        
        '''
        H = self.getH(thetas, phis, freq, pol, transmit_pol)
        G = self.getG(thetas, phis, freq, pol, transmit_pol)
        return H*G
    
    
    def getAbsoluteGain(self, AF ,freq, pol = 'ele'):
        '''
        Calculate absolute gain given noise correlation matrix and
        impedance Z0. Calculations based on document M.Arts

        Parameters
        ----------
        AF : array of complex
            Beam voltage pattern
        freq : float
            frequency Hz

        Returns
        -------
        abs_gain : array of float
            Absolute beam power

        '''
        cc, Z0 = self.getCC(freq, pol)
        ARG = np.abs(np.dot(np.dot(self.weights.conj(),cc),
                         self.weights )) /Boltzmann      
        BEP = np.abs(AF**2/self.Z0)/ARG
        abs_gain = 4 * np.pi *BEP*(120*np.pi)*freq**2/speed_of_light**2
        return abs_gain

    def getCC(self,freq, pol = 'rcp'):
        ifreq = np.argmin(np.abs(self.freq_gain - freq))
        Z0 = self.Z0
        select_pol = self.select_pol[pol]
        if pol.lower() in ['rcp','lcp','circ']:
            cc = self.cc[select_pol][:, select_pol, ifreq]
        if pol.lower() in ['hor','vert','ele']:
            cc = self.cc_lin[select_pol][:, select_pol, ifreq]
#        if pol.lower() in ['circ','ele']:
            #cc = np.eye(self.Nelem)
            #Z0 = 1
        return cc, Z0
        
    def getBeamGrid(self,freq, pol = 'rcp', transmit_pol = 'rcp'):
        AF = self.getA(*(self.spherical_grid), freq, pol = pol, 
                  transmit_pol = transmit_pol)
        w = self.weights
        e = self.gain_errors
        w = w*e
        self.beam = np.sum((AF.T*w.conj()).T,axis=0)#/np.sqrt(w.shape[0])
        return self.beam
    
    def getBeamPoint(self,theta,phi,freq, pol = 'rcp', transmit_pol = 'rcp'):
        '''

        Parameters
        ----------
        theta : float
            theta in degrees
        phi : TYPE
            phi in degrees
        freq : TYPE
            DESCRIPTION.
        pol : TYPE, optional
            DESCRIPTION. The default is 'rcp'.
        transmit_pol : TYPE, optional
            DESCRIPTION. The default is 'rcp'.

        Returns
        -------
        None.

        '''
        AF = self.getA(theta,phi, freq, pol = pol, 
                  transmit_pol = transmit_pol)
        w = self.weights
        e = self.gain_errors
        w = w*e
        return np.sum((AF.T*w.conj()).T,axis=0)#/np.sqrt(w.shape[0])
       
    
    def getHGrid(self,freq, pol = 'rcp', transmit_pol = 'rcp'):
        AF = self.getH(*(self.spherical_grid), freq, pol = pol, 
                  transmit_pol = transmit_pol)
        w = self.weights
        e = self.gain_errors
        w = w*e
        self.Hbeam = np.sum((AF.T*w.conj()).T,axis=0)#/np.sqrt(w.shape[0])
        return self.Hbeam

    def getGGrid(self,freq, pol = 'rcp', transmit_pol = 'rcp'):
        AF = self.getG(*(self.spherical_grid), freq, pol = pol, 
                    transmit_pol = transmit_pol)
        w = self.weights
        e = self.gain_errors
        w = w*e
        self.beam = np.sum((AF.T*w.conj()).T,axis=0)#/np.sqrt(w.shape[0])
        return self.beam    