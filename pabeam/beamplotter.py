#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 09:23:33 2020

@author: mevius
"""
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rcParams      #
import mpl_toolkits.mplot3d.axes3d as axes3d
import mpl_toolkits.mplot3d.art3d  as art3d #
from scipy.ndimage.filters import gaussian_filter1d
import numpy as np
import os
import math

latexfigdir = './'
 
def function_get_metaplot():        # returns plotting tuning parameters
    # purpose: define plotting tuning parameters
    # input:   -
    # returns: plotting tuning parameters
    
    class metaplot:   # defining plotting parameters (=> meta)
        def __init__(self):
            self.dummy = []        
    meta = metaplot  # define meta as class metaplot
    
    meta.xlabel = ''
    meta.ylabel = ''
    meta.title  = ''
    meta.cax_min  = None    # minimum value for the intensity colour scale (dB)
    meta.cax_max  = None    # maximum value for the intensity colour scale (dB)
    meta.col_grid = (0.0,0.0,0.0) # colour of fish eye grid (R,G,B), black
    meta.figno    = 0       # figure counter number, one increment for each plot
    meta.outputfilename = None

    # plotting parameters
    #plt.rc('text', usetex=True)       # LaTeX text rendering
    plt.rc('font', family='Times New Roman')  # set font
    #plt.jet()                         # colour scheme (hsv,hot, jet)
    plt.rcParams['font.family'] = 'Times New Roman'
    plt.rcParams['font.sans-serif'] = ['Times New Roman']
    plt.rcParams["figure.figsize"] = [9,5]      # define figure size       
    plt.rcParams.update({'font.size': 14})
    return meta



def plot_sky_eye(meta):
    THETA, PHI = np.meshgrid(meta.x,meta.y) 
    if meta.fig is None:
        
        fig = plt.figure(figsize=meta.figsize)
    else:
        fig = meta.fig
    ax = fig.add_subplot(*meta.subplot,polar = True)
    if meta.skymap.shape[0]>= PHI.shape[0]:
        meta.skymap = meta.skymap[:-1]
    if meta.skymap.shape[1]>= THETA.shape[1]:
        meta.skymap = meta.skymap[:,:-1]
    img = ax.pcolormesh(np.radians(PHI),THETA, meta.skymap,
                  cmap='viridis',shading='flat')
    #ax.set_aspect(1)
    img.set_clim(meta.cax_min,meta.cax_max)
    ax.set_xlabel(meta.xlabel,fontsize=18)
    ax.set_ylabel(meta.ylabel,fontsize=18)
    #fig.colorbar(img,orientation='vertical', fraction=0.045)
    dirandfilename = os.path.abspath(latexfigdir + meta.savefilestr)
    #fig.savefig(dirandfilename,bbo_inches = 'tight')
    return fig,ax,img
    
def plot_profile(beampower, thetas, phis, phi = 0, 
                 do_full = False, do_average = False, 
                 do_smooth = False, fig = None, ax = None, color = 'k'):

    if fig is None:
        fig = plt.figure()
    if ax  is None:
        ax = fig.add_subplot(111,)
   
    Nphi = phis.shape[0]
    if do_average:
        myprofileright = np.average(beampower[:,Nphi//2:],axis=1)
        if do_full:
            myprofilelef = np.average(beampower[1:,:Nphi//2],axis=1)
    else:
        phi_idx = np.argmin(np.abs(phi - phis))
        myprofileright = beampower[:,phi_idx]
        if do_full:
            idx2 = np.remainder(Nphi//2 + phi_idx,Nphi)
            if idx2 < phi_idx:
                myprofilelef = beampower[:,idx2]
            else:
                myprofilelef = myprofileright
                myprofileright = beampower[:,idx2]
                
    if do_full:
        y = np.concatenate((myprofilelef[::-1],myprofileright))
        thetamax = np.max(thetas)
        x = np.concatenate((-1*thetas[::-1],
                            thetas[1:]))
    else:
        x = thetas
        y = myprofileright   
    if do_smooth:
        yrsmooth = gaussian_filter1d(y, sigma=2)
    else:   
        yrsmooth = y

    ax.plot(x,yrsmooth,color)
    ax.set_xlim(x[0],x[-1])
    ax.set_xlabel("Elevation Angle (degrees)")
    ax.set_ylabel("Power (dB)")
    ax.set_title("Antenna gain profile") 
    return fig,ax

def add_sources(ax,src_dir, color = 'k'):
    '''
    add source positions to beam plot

    Parameters
    ----------
    ax : TYPE
        DESCRIPTION.
    src_dir : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    '''
    ax.scatter(np.radians(src_dir[1]),src_dir[0],s=50, linewidth = 1, 
               ec=color, fc='none')


def add_horizon(ax,color = 'w'):
    circle = plt.Circle((0., 0.), 90., transform=ax.transData._b, 
                        fill = False,
                        lw =1,
                        color = color)
    ax.add_artist(circle)

def add_colorbar(fig,img,label=''):
    fig.colorbar(img,orientation='vertical', fraction=0.045,label=label)

def function_plot_antennas(cR, figno):
    fig = plt.figure(figno)
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    cR = 100*cR
    x = cR[:,0]
    y = cR[:,1]
    z = cR[:,2]
    for xi, yi, zi in zip(x, y, z):        
        line = art3d.Line3D(*zip((xi, yi, 0), (xi, yi, zi)), marker='o', markevery=(1, 1))
        ax.add_line(line)
    
    Rx = (cR[:,0].max()-cR[:,0].min())/2 # array radius in x-direction
    Ry = (cR[:,1].max()-cR[:,1].min())/2 # array radius in y-direction
    Rz = (cR[:,2].max()-cR[:,2].min())/2 # array radius in z-direction

    Cx = (cR[:,0].max()+cR[:,0].min())/2 # array centre in x-direction
    Cy = (cR[:,1].max()+cR[:,1].min())/2 # array centre in y-direction
    Cz = (cR[:,2].max()+cR[:,2].min())/2 # array centre in z-direction
    
    scale = 1.2

    ax.set_xlim3d(Cx-scale*Rx, Cx+scale*Rx)
    ax.set_ylim3d(Cy-scale*Ry, Cy+scale*Ry)
    ax.set_zlim3d(0, Cz+scale*Rz)

    ax.set_xlabel('x (cm)', fontsize=18)
    ax.set_ylabel('y (cm)', fontsize=18)
    ax.set_zlabel('z (cm)', fontsize=18)
    plt.title('antenna positions')
    
    ax.view_init(15, 45)
    plt.locator_params(axis='y', nbins=6)
    plt.locator_params(axis='x', nbins=6)
    dirandfilename = os.path.abspath(latexfigdir + 'fig_cR' + '.pdf')
    #print(repr(dirandfilename))
    plt.savefig(dirandfilename,bbox_inches='tight')


def function_plot_array_response(thetas,phis,beam,
                                 figname = "array_response.png",
                                 label="",vmin=-13,vmax=0, fig = None,
                                 figsize = (5,5),
                                 subplotnr = (1,1,1)):
    meta = function_get_metaplot()
    meta.x = thetas
    meta.y = phis
    meta.skymap = beam
    meta.cax_min = vmin
    meta.cax_max = vmax
    #bottomstr =''
    meta.title ="beam pattern"
    #meta.title = r'beam pattern%s\\'%label + bottomstr
    #meta.xlabel = r'x = sin($\theta$)$\,$cos($\phi$)'
    #meta.ylabel = r'y = sin($\theta$)$\,$sin($\phi$)'
    meta.savefilestr = figname
    meta.subplot = subplotnr
    meta.fig = fig
    meta.figsize = figsize
    fig,ax,img = plot_sky_eye(meta)
    return fig,ax,img
 

def plotBeam(beam, freq, receiver_pol = 'ele', transmit_pol = 'rcp', 
             label = '', vmin = -20, vmax = 13, scale_beam = None):
    raw_beam = beam.getBeamGrid(freq = freq, pol = receiver_pol,
                                transmit_pol = transmit_pol )
    beampower = 10.*np.log10(beam.getAbsoluteGain(freq =  freq,
                                                  AF = raw_beam,
                                                  pol = receiver_pol))
    if not scale_beam is None:
        beampower +=  scale_beam - np.max(beampower)
    fig,ax,img = function_plot_array_response (beam.thetas, 
                                               beam.phis,beampower.T,
                                               label = label,
                                               vmin = vmin, 
                                               vmax = vmax)
    return fig,ax,img,beampower

def sphere2cart(theta,phi):
    '''
    convert spherical coordinates to cartesian unit coordinates

    Parameters
    ----------
    theta : (array of) float
        polar angle theta (w.r.t. zenith) in rad
    phi : (array of) float
        azimuth angle phi in rad

    Returns
    -------
    array of float
         cartesian direction vectors S, dim.(3,) + shape (theta)   

    '''
    
    return np.array([np.cos(phi)*np.sin(theta),
                     np.sin(phi)*np.sin(theta),
                     np.cos(theta)])
    
# narrow band condition, depth of nulls when nulling at f offsets
def function_plot_filter_bw_effectiveness():
    def function_get_filter_effectiveness(S,f,df,Nf):
        #S  = np.array([np.sqrt(0.25),0,np.sqrt(0.75)])     # (3,)
        k1 = (1/np.sqrt(7))
        k3 = np.matmul(par.cR,S)             # (Nant,1)
        a_f  = k1*np.exp(-2*pi*1j*(f/c)*k3)  # (Nant,)
        tmp1 = np.outer(a_f,np.conj(a_f))
        tmp2 = np.inner(a_f,np.conj(a_f))
        P    = np.eye(par.Nant) - tmp1/tmp2  # (Nant,Nant)
        eta  = np.zeros(Nf)                  # (Nf,)
        Rf   = np.outer(a_f,np.conj(a_f))
        for k in range(Nf):
            a_df   = k1 * np.exp(-2*pi*1j*(df[k]/c)*k3)    # (Nant,)
            R      = np.outer(a_f*a_df,np.conj(a_f*a_df))  # (Nant,Nant)
            Rfilt  = np.matmul(P,np.matmul(R,P))
            eta[k] = np.linalg.norm(Rfilt, ord='fro')      # (Nf,)
        eta = 10*np.log10(eta)
        return eta

    f  = 1.6e9                           # ()
    Nf = 60
    df = np.logspace(6,9,Nf)         # (Nf,)
    Ntraces = 100
    theta = np.random.random(Ntraces)*90
    phi   = np.random.random(Ntraces)*360
    S = sphere2cart(np.radians(theta),np.radians(phi))

    fig= plt.figure(figsize=(5,4))
    for k in range(Ntraces):
        eta = function_get_filter_effectiveness(S[:,k],f,df,Nf)
        plt.semilogx(df/1e6,eta,'k',linewidth=0.1)
    plt.grid()
    xmin, xmax, ymin, ymax = plt.axis([1,300,-70,0])
    plt.xlabel('frequency offset (MHz)')
    formulastr = '10 log$_{10}$($||\mathbf{P}_{\!\\nu}\mathbf{R}_{\!\\nu+\\Delta\\nu}^o\mathbf{P}_{\!\\nu}||_F^2$)'
    plt.ylabel(formulastr + ' (dB)')

    
    plt.title('filter attenuation effectiveness')
    dirandfilename = os.path.abspath(latexfigdir + 'fig_bw' + '.pdf')
    plt.savefig(dirandfilename,bbox_inches='tight')
