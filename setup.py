#!/usr/bin/env python
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()
if __name__ == "__main__":
    setuptools.setup(
        name="pabeam",
        version="0.0.1",
        author="Mevius",
        author_email="mevius@astron.nl",
        description="A python package for simulating phased arrays",
        long_description=long_description,
        long_description_content_type="text/markdown",
        packages=['pabeam'],
    )
